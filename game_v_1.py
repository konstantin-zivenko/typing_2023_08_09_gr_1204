import random
from typing import List, Tuple

SUITS = "♠ ♡ ♢ ♣".split()  # создаем список мастей
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()  # создаем список рангов

Card = Tuple[str, str]
Deck = List[Card]


def create_deck(shuffle=False) -> Deck:
    """Создаем новую колоду. При необходимости - тасуем (shuffle=True)"""
    deck = [(s, r) for r in RANKS for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck


def deal_hands(deck: Deck) -> Tuple[Deck, Deck, Deck, Deck]:
    """сдает колоду четырем игрокам"""
    return (deck[0::4], deck[1::4], deck[2::4], deck[3::4])


def play() -> None:
    """реализует цикл игры: создает колоду, тасует, раздает четырм игрокам"""
    deck = create_deck(shuffle=True)
    names = "P1 P2 P3 P4".split()
    hands = {n: h for n, h in zip(names, deal_hands(deck))}

    for name, cards in hands.items():
        card_str = " ".join(f"{s}{r}" for (s, r) in cards)
        print(f"{name}: {card_str}")


if __name__ == "__main__":
    play()
