import random
from typing import NoReturn, Sequence, Any


def play(player_name: str) -> None:
    print(f"{player_name} plays")


def black_hole() -> NoReturn:
    raise Exception("There is no going back...")


def choose(items: Sequence[Any]) -> Any:
    """выбирает и возвращает случайный элемент из набора"""
    return random.choice(items)


names = ["Radomir", "Sergyi", "Rita"]
reveal_type(names)
