"""PEP 544"""

from typing import Container, Iterable, Awaitable, ContextManager, Optional
from typing_extensions import Protocol

# class Sized(Protocol):
#     def __len__(self): -> int: ...
#
# def len(obj: Sized) -> int:
#     return obj.__len__()


def add(li: Optional[list] = None) -> list:
    if li is None:
        li = []
    li.append("###")
    print(li)